Name: ORBit2
Version: 2.14.19
Release: 26
Summary: A high-performance CORBA Object Request Broker
License: LGPLv2+ and GPLv2+
URL: https://gitlab.gnome.org/Archive/orbit2
Source: http://download.gnome.org/sources/ORBit2/2.14/%{name}-%{version}.tar.bz2
BuildRequires: autoconf automake gtk-doc libtool
BuildRequires: glib2-devel >= 2.2.0
BuildRequires: libIDL-devel >= 0.8.2-1
BuildRequires: pkgconfig >= 0.14

Patch0: ORBit2-2.14.3-multilib.patch
Patch1: ORBit2-2.14.3-ref-leaks.patch
Patch2: ORBit2-make-j-safety.patch
Patch3: ORBit2-allow-deprecated.patch

Patch6000: 0005-Fix-link_protocol_is_local_ipv46-for-ipv4-on-some-ip.patch
Patch6001: 0014-Commit-patch-from-bug-662623-to-fix-connection-failu.patch
Patch6002: 0016-bug-732274-fix-parallel-build-failure.patch

%description
ORBit is an efficient, free C-based ORB, compliant to CORBA version 2.2. CORBA 
stands for Common Object Request Broker Architecture. CORBA in many ways is a 
successor to the Remote Procedure Call (RPC) system common on UNIX and other 
systems: the best oneline description of CORBA is that it is "Object Orientated 
RPC".
The key point about CORBA is that it provides a way for two programs to 
communicate information. The CORBA mechanism allows these two programs to be 
running on different machines and written in different programming languages 
while safely (and portably) exchanging data. They also could be running in the 
same program, on the same machine, in which case the process of communication is 
much quickeras ORBit recognises that it does not need to open any communication 
channel.

%package devel
Summary: Development libraries, header files and utilities for ORBit
Requires: %{name} = %{version}-%{release}
Requires: automake indent pkgconfig
Requires: libIDL-devel >= 0.8.2-1
Requires: glib2-devel >= 2.2.0
Conflicts: ORBit-devel <= 1:0.5.8

%description devel
Development package for ORBit.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-gtk-doc --enable-purify --disable-static
%make_build

%install
rm -rf $RPM_BUILD_ROOT
%make_install

%ldconfig_scriptlets

%files
%{_libdir}/*.so.*
%{_libdir}/orbit-2.0/*.so*
%doc AUTHORS COPYING README TODO
%exclude %{_libdir}/*.la
%exclude %{_libdir}/ORBit-2.0/*.*a
%exclude %{_libdir}/orbit-2.0/*.*a

%files devel
%{_bindir}/ior-decode-2
%{_bindir}/linc-cleanup-sockets
%{_bindir}/orbit-idl-2
%{_bindir}/orbit2-config
%{_bindir}/typelib-dump
%{_libdir}/*.so
%{_libdir}/libname-server-2.a
%{_libdir}/pkgconfig/*
%{_datadir}/aclocal/*
%{_datadir}/gtk-doc
%{_datadir}/idl/orbit-2.0
%{_includedir}/*

%changelog
* Tue Apr 12 2022 hanhui <hanhui15@h-partners.com> - 2.14.19-26
- DESC: fix build failure due to autoconf upgrade

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.14.19-25
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Thu Dec 17 2020 xinghe <xinghe1@huawei.com> - 2.14.19-24
- correct url

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.14.19-23
- Fix ldconfig scriptlets

* Thu Dec 19 2019 YunFeng Ye <yeyunfeng@huawei.com> - 2.14.19-22
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:backport bugfix patch

* Mon Aug 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.14.19-21
- Package init
